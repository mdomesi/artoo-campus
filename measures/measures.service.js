angular.module('artoo').factory('Measures', function() {
  //misurazione per tipologia di capo
  var measures = {
    typology: 'top',
    name: 't-shirt',
    sizes: [{
      size: 'M',
      measures: [{
        type: 'shoulder',
        min: 0,
        max: 0
      }, {
        type: 'normal waist',
        min: 0,
        max: 0
      }]
    }]
  };

  var getMeasures = function() {
    return measures;
  };

  return {
    getMeasures: getMeasures
  };
});